///drawPlayer()
//Draws the player including whitemasks

if weapon == 0
{
    drawSelf();
}
else
{
    var primary, secondary, static, primaryCol, secondaryCol;

    //Decide on the masks
    switch sprite_index
    {
        case sprMegamanStand:
            primary = sprMegamanStandPrimary;
            secondary = sprMegamanStandSecondary;
            static = sprMegamanStandStatic;
        break;
        
        case sprMegamanStep:
            primary = sprMegamanStepPrimary;
            secondary = sprMegamanStepSecondary;
            static = sprMegamanStepStatic;
        break;
        
        case sprMegamanJump:
            primary = sprMegamanJumpPrimary;
            secondary = sprMegamanJumpSecondary;
            static = sprMegamanJumpStatic;
        break;
        
        case sprMegamanWalk:
            primary = sprMegamanWalkPrimary;
            secondary = sprMegamanWalkSecondary;
            static = sprMegamanWalkStatic;
        break;
        
        case sprMegamanStandShoot:
            primary = sprMegamanStandShootPrimary;
            secondary = sprMegamanStandShootSecondary;
            static = sprMegamanStandShootStatic;
        break;
        
        case sprMegamanJumpShoot:
            primary = sprMegamanJumpShootPrimary;
            secondary = sprMegamanJumpShootSecondary;
            static = sprMegamanJumpShootStatic;
        break;
        
        case sprMegamanWalkShoot:
            primary = sprMegamanWalkShootPrimary;
            secondary = sprMegamanWalkShootSecondary;
            static = sprMegamanWalkShootStatic;
        break;
        
        case sprMegamanSlide:
            primary = sprMegamanSlidePrimary;
            secondary = sprMegamanSlideSecondary;
            static = sprMegamanSlideStatic;
        break;
        
        default:
            primary = sprMegamanStandPrimary;
            secondary = sprMegamanStandSecondary;
            static = sprMegamanStandStatic;
        break;
    }
    
    
    //Decide on the color
    switch weapon
    {
        case 1:
            primaryCol = make_color_rgb(47, 9, 197);
            secondaryCol = make_color_rgb(200, 100, 0);
        break;
        
        case 2:
            primaryCol = make_color_rgb(180, 190, 46);
            secondaryCol = make_color_rgb(0, 255, 0);
        break;
    }
    
    
    draw_sprite_ext(primary, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, primaryCol, image_alpha);
    draw_sprite_ext(secondary, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, secondaryCol, image_alpha);
    draw_sprite_ext(static, image_index, round(x), round(y), image_xscale, image_yscale, image_angle, c_white, image_alpha);
}
