grav = 0.5;
hsp = 0;
vsp = 0;
platform=false;
imjumping=false;
jumpspeed_normal = 7;
jumpspeed_powerup = 10;

jumpspeed = jumpspeed_normal;

movespeed = 4;

// IA checks
hcollision = false;
fearoftheheights = false;

//Constants
grounded = false;
jumping = false;

//Horizontal jump constants
hsp_jump_constant_small = 1;
hsp_jump_constant_big = 4;
hsp_jump_applied = 0;

//Horizontal key pressed count
hkp_count = 0;
hkp_count_small = 2;
hkp_count_big = 5;

//Init variables
key_left = 0;
key_right = 0;
key_jump = false;
key_shoot = false;
