move = key_left + key_right;

if(!platform){
    hsp = move*movespeed;
}
if(vsp<10)vsp+=grav;


if(place_meeting(x,y+1,objSolid))
{
    //Check if recently grounded
    if(grounded && !key_jump){
        hkp_count = 0; //Init horizontal count
        jumping = false;
    }else if(grounded && key_jump){ //recently jumping
        jumping = true;
    }

    //Check if player grounded
    grounded = !key_jump;
    
    vsp = key_jump * -jumpspeed;
}

//Init hsp_jump_applied
if(grounded){
    hsp_jump_applied = 0;
}

//Check horizontal counts
if(move!=0 && grounded){
 hkp_count++;
}else if(move==0 && grounded){
 hkp_count=0;
}

//Check jumping
if(jumping){
    
    //check if previously we have jump
   // if(hsp_jump_applied == 0){
        hsp_jump_applied = sign(move);       
    //}

    //don't jump horizontal
    /*if(hkp_count < hkp_count_small ){
        // no hsp jump
        hsp = 0;
    }else if(hkp_count >= hkp_count_small && hkp_count < hkp_count_big){ 
        //small jump
        hsp = hsp_jump_applied * hsp_jump_constant_small;
    }else{ 
        // big jump
        hsp = hsp_jump_applied *hsp_jump_constant_big
    }*/
}

//horizontal collision
if(place_meeting(x+hsp,y,objSolid))
{
    while(!place_meeting(x+sign(hsp),y,objSolid))
    {
        x+= sign(hsp);
    }
    hsp = 0;
    hcollision = true;
}

x+= hsp;

//vertical collision
if(place_meeting(x,y+vsp,objSolid))
{
    while(!place_meeting(x, sign(vsp) + y,objSolid))
    {
        y+= sign(vsp);
    }
    vsp = 0;
    grounded=true;
}

y+= vsp;

//ground collision
if (place_meeting (x, y+8, objSolid))
    { 
       if (fearoftheheights && position_meeting(x+(96)*dir, y+(sprite_height/2)+8, objSolid) && !position_meeting(x+(sprite_width/2)*dir, y+(sprite_height/2)+8, objSolid))
            {
                key_jump = true;
            } 
            else if (fearoftheheights && !position_meeting(x+(sprite_width/2)*dir, y+(sprite_height/2)+8, objSolid))
            {
                dir *= -1;
            }
        }
        
        
if(grounded){
    imjumping=false;
   }
   else{
     imjumping=true;
   }        
    
